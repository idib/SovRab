class Init < ActiveRecord::Migration[5.1]
  def change
    create_table :teches do |t|
      t.string :name
      t.integer :price
      t.integer :weight
      t.text :description
      t.integer :year
      t.integer :hours_use
      t.integer :mileage_use
      t.integer :volume_dvs
      t.integer :power

      t.timestamps
    end

    create_table :categories do |t|
      t.string :name

      t.timestamps
    end

    create_table :vendors do |t|
      t.string :name

      t.timestamps
    end

    create_table :models do |t|
      t.string :name

      t.timestamps
    end

    create_table :type_adverts do |t|
      t.string :name

      t.timestamps
    end

    create_table :states do |t|
      t.string :name

      t.timestamps
    end


    add_reference :teches, :category, foreign_key: true

    add_reference :teches, :model, foreign_key: true

    add_reference :teches, :state, foreign_key: true

    add_reference :teches, :type_advert, foreign_key: true

    add_reference :models, :vendor, foreign_key: true
  end
end
