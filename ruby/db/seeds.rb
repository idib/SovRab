# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


cat = Category.create([
                          {name: 'Автокраны'},
                          {name: 'Бульдозеры'},
                          {name: 'Автоподемники'},
                          {name: 'Экскаваторы'},
                          {name: 'Сельхоз-техника'},
                          {name: 'Коммунальная'},
                          {name: 'Самопогрузчики'},
                          {name: 'Строительная техника'}])

ven = Vendor.create([
                        {name: 'КАМАЗ'},
                        {name: 'МАЗ'},
                        {name: 'Volvo'},
                        {name: 'Komatsu'}])

mod = Model.create([
                       {name: 'Камаз 4308', vendor: ven[0]},
                       {name: 'Камаз 4310', vendor: ven[0]},
                       {name: 'МАЗ 975800', vendor: ven[1]},
                       {name: 'МАЗ 6430', vendor: ven[1]},
                       {name: 'Volvo B', vendor: ven[2]},
                       {name: 'Volvo EC', vendor: ven[2]},
                       {name: 'Komatsu WB93R', vendor: ven[3]},
                       {name: 'Komatsu LW250', vendor: ven[3]}])

type = TypeAdvert.create([
                             {name: 'Продам'},
                             {name: 'Куплю'},
                             {name: 'Сдам в аренду'}])
stat = State.create([
                        {name: 'Новая'},
                        {name: 'Хорошее'},
                        {name: 'Удовлетворительное'},
                        {name: 'После ДТП'}
                    ])

Tech.create([
                {name: 'assdsa', price: 5165, weight: 3000, description: 'asfasfasfasfadf dfasdfasdfasdf',
                 year: 2005, mileage_use: 5, volume_dvs: 5000, power: 400, category: cat[0], model: mod[0],
                 type_advert: type[0],state: stat[0]},
                {name: 'assasfasfdsa', price: 5165, weight: 3000, description: 'asfasfasfasfadf dfasdfasdfasdf',
                 year: 2005, mileage_use: 5, volume_dvs: 5000, power: 400, category: cat[1], model: mod[1],
                 type_advert: type[1],state: stat[1]},
                {name: 'assasfasfdsa', price: 5165, weight: 3000, description: 'asfasfasfasfadf dfasdfasdfasdf',
                 year: 2005, mileage_use: 5, volume_dvs: 5000, power: 400, category: cat[1], model: mod[2],
                 type_advert: type[1],state: stat[2]},
            ])
=begin
=end