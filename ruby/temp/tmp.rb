# require 'mechanize'
#
# agent = Mechanize.new
#
#
# agent.set_proxy("m3.mbmproxy.com", 53201)
#
# # page = agent.get('https://aptekamos.ru/apteka/price.html?mt=2&mc=67639&num=0&r=0&country=0&vender=0&page=1&zakaz=0&duty=0&exp=0&apteka_name=&netapteki=0&dist=0&street=0&metro=0')
# page = agent.get('https://aptekamos.ru/kupit/spisok?l=1&mt=2&mc=67639&num=0&vender=0&country=0&metro=0')
# # page = agent.get("http://localhost:4567")
#
# name = page.search('.trade-name').text
# int_name = page.search('.med-inn').text
# vendor = page.search('.med-firm').text
# cargo = page.search('.med-form').text
# num = page.search('.pack-num').text
# paging = page.search('#d-table-pager-text').text
#
# pos = page.search('#ret tbody tr').map do |el|
#   idd = el.attributes['data-org-id'].to_s
#   pharm_name = el.search('.ret-td-org .bold-text').text
#   price = el.search('.ret-drug-price').inner_html
#   {
#       idd: idd,
#       name: pharm_name.gsub('Об аптеке', '').gsub('"',"").strip,
#       price: price.to_f
#   }
# end
#
# puts name
# puts int_name
# puts vendor
# puts pos
#
#
#
#
# require 'rubygems'
# require 'nokogiri'
# require 'open-uri'
#
#

crafted_url = "https://aptekamos.ru/apteka/price.html?mt=2&mc=54078&page=1&r=0&vender=0&country=0&vender=0&page=1&zakaz=0&duty=0&exp=0&apteka_name=&netapteki=0&dist=0&street=0&metro=0"

# crafted_url = 'https://aptekamos.ru/apteka/drug_k.html?med_id=67639&r=0'#'https://aptekamos.ru/apteka/price.html?mt=2&mc=67639&num=0&r=0&country=0&vender=0&page=1&zakaz=0&duty=0&exp=0&apteka_name=&netapteki=0&dist=0&street=0&metro=0'


# doc = Nokogiri::HTML(open(crafted_url, proxy: 'http://m3.mbmproxy.com:53201'), nil, Encoding::UTF_8.to_s)
# name = doc.css('.trade-name').text
# int_name = doc.css('.med-inn').text
# vendor = doc.css('.med-firm').text
#
# puts name
# puts int_name
# puts vendor
#
#

require 'wombat'

Wombat.configure do |config|
  config.set_proxy "m3.mbmproxy.com", 53201
  config.set_user_agent "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
  # config.set_user_agent_alias 'Windows Mozilla'
end

result = Wombat.crawl do
  base_url crafted_url
  name xpath: '//*[@class="trade-name"]'
  int_name css: '.med-inn'
  vendor css: '.med-firm'
  cargo xpath: '//*[@class="med-form"]'
  numm xpath: '//*[@class="pack-num"]'
  paging xpath: '//*[@id="d-table-pager-text"]'
end

puts result['name']
puts result['int_name']
puts result['vendor']