class TechesController < ApplicationController

  before_action :set_tech, only: [:show, :edit, :update, :destroy]

  def index
    @teches = Tech.all
  end

  def show

  end

  def new
    @tech = Tech.new
  end

  def edit

  end

  def create
    @tech = Tech.new(tech_params)
    if @tech.save
      redirect_to tech_path(@tech), flash: {success: "Добавленно"}
    else
      @error = @tech.errors.full_messages.first
      
      render action: 'new' and @error
    end
  end

  def update
    if @tech.update(tech_params)
      redirect_to tech_path(@tech), flash: {success: 'Обновление прошло успешно'}
    else
      @error = @scheme.errors.full_messages.first
      render action: 'edit' and @error
    end
  end

  def destroy
    begin
      @tech.destroy
      redirect_to teches_path, flash: {success: 'Удалено'}
    rescue
      redirect_to tech_path(@tech), flash: {error: 'Удалить не возможно'}
    end
  end

  private

  def tech_params
    params.require(:tech).permit(:name, :price, :description, :weight, :year, :hours_use, :mileage_use,
                                 :volume_dvs, :power, :category_id, :model_id, :state_id, :type_advert_id)
  end

  def set_tech
    @tech = Tech.find(params[:id])
  end

end
