class CategoriesController < ApplicationController

  before_action :set_category, only: [:show, :edit, :update, :destroy]

  def index
    @categories = Category.all
    @category_count = Category.joins(:teches).group('id').count
  end

  def show
    @teches = Tech.where(category: @category)
  end

  def new
    @category = Category.new
  end

  def edit

  end

  def create
    @category = Category.new(category_params)
    if @category.save
      redirect_to category_path(@category), flash: {success: "Добавленно"}
    else
      @error = @category.errors.full_messages.first
      render action: 'new' and @error
    end
  end

  def update
    if @category.update(category_params)
      redirect_to category_path(@category), flash: {success: 'Обновление прошло успешно'}
    else
      render action: 'edit' and @scheme.errors.full_messages.first
    end
  end

  def destroy
    begin
      @category.destroy
      redirect_to categories_path, flash: {success: 'Удалено'}
    rescue
      redirect_to category_path(@category), flash: {error: @scheme.errors.full_messages.first}
    end
  end

  private

  def category_params
    params.require(:category).permit(:name)
  end

  def set_category
    @category = Category.find(params[:id])
  end

end
