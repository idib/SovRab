class VendorsController < ApplicationController

  before_action :set_vendor, only: [:show, :edit, :update, :destroy]

  def index
    @vendors = Vendor.all
    @vendor_count = Vendor.joins(models: :teches).group('id').count
  end

  def show
    @models = Model.where(vendor: @vendor)
    @teches = Tech.joins(:model).where(models: {vendor: @vendor})
  end

  def new
    @vendor = Vendor.new
  end

  def edit

  end

  def create
    @vendor = Vendor.new(vendor_params)
    if @vendor.save
      redirect_to vendor_path(@vendor), flash: {success: "Добавленно"}
    else
      @error = @vendor.errors.full_messages.first
      render action: 'new' and @error
    end
  end

  def update
    if @vendor.update(vendor_params)
      redirect_to vendor_path(@vendor), flash: {success: 'Обновление прошло успешно'}
    else
      @error = @scheme.errors.full_messages.first
      render action: 'edit' and @error
    end
  end

  def destroy
    begin
      @vendor.destroy
      redirect_to vendors_path, flash: {success: 'Удалено'}
    rescue
      redirect_to vendor_path(@vendor), flash: {error: 'Удалить не возможно'}
    end
  end

  private

  def vendor_params
    params.require(:vendor).permit(:name)
  end

  def set_vendor
    @vendor = Vendor.find(params[:id])
  end
end
