class HomeController < ApplicationController

  def index
    @teches = Tech.last 20

    @vendor_count = Vendor.joins(models: :teches).group('id').count
    @vendors = Vendor.all

  end
end
