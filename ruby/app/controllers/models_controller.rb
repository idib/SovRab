class ModelsController < ApplicationController

  before_action :set_vendor, only: [:show, :edit, :update, :destroy]

  def index
    @vendorModels = Hash.new
    @vendors = Vendor.all
    @vendors.each do |v|
      @vendorModels[v] = Model.where(vendor: v)
    end
    @techCount = Model.joins(:teches).group(:id).count
  end

  def show
    @teches = Tech.where(model: @model)
  end

  def new
    @model = Model.new(vendor_id: params[:vendor_id])
  end

  def edit

  end

  def create
    @model = Model.new(model_params)
    if @model.save
      redirect_to model_path(@model), flash: {success: "Добавленно"}
    else
      @error = @model.errors.full_messages.first
      render action: 'new' and @error
    end
  end

  def update
    if @model.update(model_params)
      redirect_to model_path(@model), flash: {success: 'Обновление прошло успешно'}
    else
      @error = @scheme.errors.full_messages.first
      render action: 'edit' and @error
    end
  end

  def destroy
    begin
      @model.destroy
      redirect_to models_path, flash: {success: 'Удалено'}
    rescue
      redirect_to model_path(@model), flash: {error: 'Удалить не возможно'}
    end
  end

  private

  def model_params
    params.require(:model).permit(:name, :vendor_id)
  end

  def set_vendor
    @model = Model.find(params[:id])
  end

end
