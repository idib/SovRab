class TypeAdvertsController < ApplicationController
  before_action :set_type_advert, only: [:show, :edit, :update, :destroy]

  def index
    @typeAdverts = TypeAdvert.all
    @type_advert_count = TypeAdvert.joins(:teches).group('id').count
  end

  def show
    @teches = Tech.where(type_advert: @typeAdvert)
  end

  def new
    @typeAdvert = TypeAdvert.new
  end

  def edit

  end

  def create
    @typeAdvert = TypeAdvert.new(type_adverts_params)
    if @typeAdvert.save
      redirect_to type_adverts_path(@typeAdvert), flash: {success: "Добавленно"}
    else
      @error = @typeAdvert.errors.full_messages.first
      render action:  'new' and @error
    end
  end

  def update
    if @typeAdvert.update(type_adverts_params)
      redirect_to type_advert_path(@typeAdvert), flash: {success: 'Обновление прошло успешно'}
    else
      @error = @scheme.errors.full_messages.first
      render action: 'edit' and @error
    end
  end

  def destroy
    begin
      @typeAdvert.destroy
      redirect_to type_adverts_path, flash: {success: 'Удалено'}
    rescue
      redirect_to type_advert_path(@typeAdvert), flash: {error: 'Удалить не возможно'}
    end
  end

  private

  def type_adverts_params
    params.require(:typeAdvert).permit(:name)
  end

  def set_type_advert
    @typeAdvert = TypeAdvert.find(params[:id])
  end

end
