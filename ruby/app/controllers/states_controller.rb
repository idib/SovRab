class StatesController < ApplicationController
  before_action :set_state, only: [:show, :edit, :update, :destroy]

  def index
    @states = State.all
    @state_count = State.joins(:teches).group('id').count
  end

  def show
    @teches = Tech.where(state: @state)
  end

  def new
    @state = State.new
  end

  def edit

  end

  def create
    @state = State.new(state_params)
    if @state.save
      redirect_to state_path(@state), flash: {success: "Добавленно"}
    else
      @error = @state.errors.full_messages.first
      render action: 'new' and @error
    end
  end

  def update
    if @state.update(state_params)
      redirect_to state_path(@state), flash: {success: 'Обновление прошло успешно'}
    else
      @error = @scheme.errors.full_messages.first
      render action: 'edit' and @error
    end
  end

  def destroy
    begin
      @state.destroy
      redirect_to states_path, flash: {success: 'Удалено'}
    rescue
      redirect_to state_path(@state), flash: {error: 'Удалить не возможно'}
    end
  end

  private

  def state_params
    params.require(:state).permit(:name)
  end

  def set_state
    @state = State.find(params[:id])
  end

end
