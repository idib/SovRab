class Tech < ApplicationRecord
  belongs_to :category
  belongs_to :model
  belongs_to :state
  belongs_to :type_advert
end
