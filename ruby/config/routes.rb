Rails.application.routes.draw do
  get 'vendors/Models'

  get 'vendors/States'

  get 'vendors/TypeAdverts'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :teches
  resources :categories
  resources :vendors
  resources :models
  resources :states
  resources :type_adverts

  root to: 'home#index'
end
